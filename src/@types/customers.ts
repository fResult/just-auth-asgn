export interface ICustomer {
  id: number;
  group_id: number;
  default_billing: string;
  default_shipping: string;
  created_at: string;
  updated_at: string;
  created_in: string;
  dob: string;
  email: string;
  firstname: string;
  lastname: string;
  store_id: number;
  website_id: number;
  addresses: IAddress[];
  disable_auto_group_change: number;
  extension_attributes: IExtensionAttrs;
}

export interface IRegion {
  region_code: string;
  region: string;
  region_id: number;
}

export interface IAddress {
  id: number;
  customer_id: number;
  region: IRegion;
  region_id: number;
  country_id: string;
  street: string[];
  telephone: string;
  postcode: string;
  city: string;
  firstname: string;
  lastname: string;
  default_shipping: boolean;
  default_billing: boolean;
}

export interface IExtensionAttrs {
  is_subscribed: boolean;
}
