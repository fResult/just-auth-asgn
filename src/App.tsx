import React, { FC } from 'react'
import { Link, useRoutes } from 'react-router-dom'
import type { RouteObject } from 'react-router-dom'
import Customer from './pages/Customer'
import Home from './pages/Home'
import Login from './pages/Login'
import NotFound from './pages/NotFound'

const privateRoutes: RouteObject[] = [
  {
    path: '/customers/me',
    element: <Customer />,
  },
]
const publicRoutes: RouteObject[] = [
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/sign-in',
    element: <Login />,
  },
]

const routes: RouteObject[] = [
  ...publicRoutes,
  ...privateRoutes,
  {
    path: '*',
    element: <NotFound />,
  },
]

type AppProps = {
  className?: string
}

const App: FC<AppProps> = ({ className = '' }) => {
  const routeElements = useRoutes(routes)
  return <>{routeElements}</>
}

export default App
