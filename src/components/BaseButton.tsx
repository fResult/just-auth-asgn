import { Button, ButtonTypeMap } from '@mui/material'
import React, { ButtonHTMLAttributes, FC, ReactNode } from 'react';
import styled from 'styled-components'

type BaseButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  className?: string
  children: ReactNode
}

const StyledButton = styled(Button)<BaseButtonProps>/* css */`
  padding: '0.5rem 1rem';
  background-color: dodgerblue;
  color: #fff;

  &:hover {
    background-color: blueviolet;
  }
`

const BaseButton: FC<BaseButtonProps> = ({
  children,
  ...props
}) => {

  return (
    <StyledButton {...props as ButtonTypeMap<ButtonHTMLAttributes<HTMLButtonElement>, "button">}>
      {children}
    </StyledButton>
  );
};

export default BaseButton;
