import axios, { AxiosRequestConfig } from "axios";

const axiosOption: AxiosRequestConfig<any> = {
  // ! TODO: Use API URL
  baseURL: process.env.BASE_API_URL,
}

axios.interceptors.request.use(function configInterceptor(config) {
  return {
    ...config,
    // ...axiosOption
    baseURL: axiosOption.baseURL
  }
})



export default axios