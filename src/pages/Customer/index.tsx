import type { AxiosError } from 'axios'
import { useNavigate } from 'react-router-dom'

import {
  Card,
  CardContent,
  CardHeader,
  Typography,
} from '@mui/material'
import React, {
  FC,
  Fragment,
  useMemo,
  useRef,
  useState,
  useEffect,
  useLayoutEffect,
} from 'react'
import { fetchCustomerDetail } from '../../services/api/customers'
import { useCustomerStore } from '../../store/customers'
import { ICustomer } from '../../@types/customers'

type CustomerProps = {
  className?: string
}

const Customer: FC<CustomerProps> = ({ className = '' }) => {
  const { token, setToken, customer, setCustomer } = useCustomerStore()
  const [isLoading, setIsLoading] = useState(false)

  const addressListRef = useRef<HTMLDivElement[]>([])
  const addressesRef = useRef<HTMLUListElement>(null)

  const navigate = useNavigate()

  useEffect(() => {
    if (!addressListRef.current) return
    addressListRef.current = addressListRef.current.slice(
      0,
      customer?.addresses.length || 0,
    )
  }, [customer?.addresses.length])

  useLayoutEffect(() => {
    addressListRef.current.forEach((addrRef, idx) => {
      console.log('test addr ref', addrRef)
      if (idx % 2 === 0) {
        customer?.addresses?.[idx]?.default_billing && addrRef?.focus()
      }
    })
  }, [addressListRef.current, customer?.addresses])

  useEffect(() => {
    handleFetchCustomer(token)
  }, [token, setToken])

  function handleUnauthorizedAccess() {
    alert('❌ You are not authorized, please login')
    localStorage.removeItem('token')
    navigate('/sign-in')
  }

  async function handleFetchCustomer(token: string) {
    if (!token) {
      const tokenFromStorage = localStorage.getItem('token') || ''
      if (!tokenFromStorage) handleUnauthorizedAccess()
      setToken(tokenFromStorage)
      return
    }

    setIsLoading(true)
    try {
      const customerResp = await fetchCustomerDetail(token)
      setCustomer(customerResp)
    } catch (error: unknown) {
      console.error('❌', error)
      const axiosError = error as AxiosError<ICustomer>
      if (axiosError.response?.status === 401) {
        handleUnauthorizedAccess()
      }
    } finally {
      setIsLoading(false)
    }
  }

  const cardValues = useMemo(() => {
    if (!customer) return []
    return [
      {
        label: 'Name',
        value: [customer.firstname, customer.lastname].join(' '),
      },
      { label: 'Email', value: customer.email },
      { label: 'Date of Birth', value: customer.dob },
      { label: 'Addresses', children: customer.addresses },
    ]
  }, [customer])

  return (
    <main className={className}>
      {isLoading ? (
        <p>...Loading...</p>
      ) : !customer ? (
        <p>User Not Found</p>
      ) : (
        <Card sx={{ minWidth: 250 }}>
          <CardHeader title="My Info" />
          <CardContent
            sx={{
              display: 'grid',
              gridTemplateColumns: 'repeat(2, 1fr)',
            }}
          >
            {cardValues.map(({ label, value, children = [] }, idx) => {
              return (
                <Fragment key={idx}>
                  <Typography key={idx} className="card-label">{label} </Typography>

                  {!children.length ? (
                    <Typography className="card-value">{value}</Typography>
                  ) : (
                    <section className="address">
                      <ul
                        style={{
                          listStyle: 'none',
                          display: 'flex',
                          flexDirection: 'column',
                          rowGap: '1rem',
                        }}
                        ref={addressesRef}
                      >
                        {children.map((addr, i) => {
                          return (
                            <li key={addr.id}>
                              <Card
                                tabIndex={0}
                                ref={(el) => (addressListRef.current[i] = el!)}
                                sx={{
                                  cursor: 'pointer',
                                  display: 'grid',
                                  gridTemplateColumns: 'repeat(2, 1fr)',
                                  rowGap: '0.25rem',
                                }}
                                onFocus={() => console.log('focus', [addr.firstname, addr.lastname].join(' '))}
                                onClick={(e) => e.preventDefault()}
                                // onBlur={() => addressListRef.current[i].style.outline = 'none'}
                              >
                                <Typography
                                  className="card-label"
                                  style={{ width: 'auto' }}
                                >
                                  Recipient:
                                </Typography>
                                <Typography className="card-value">
                                  {[addr.firstname, addr.lastname].join(
                                    ' ',
                                  )}
                                </Typography>
                                <Typography
                                  className="card-label"
                                  style={{ width: 'auto' }}
                                >
                                  Street:
                                </Typography>
                                <Typography className="card-value">
                                  {addr.street}
                                </Typography>
                              </Card>
                            </li>
                          )
                        })}
                      </ul>
                    </section>
                  )}
                </Fragment>
              )
            })}
          </CardContent>
        </Card>
      )}
    </main>
  )
}

export default Customer
