import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Button from '@mui/material/Button'

type HomeProps = {
  className?: string
}

const LinkButton = styled(Button)/* css */ `
  border: 1px solid #333;
  background-color: #333;
  padding: 8px 16px;
  border-radius: 0.5rem;
  text-decoration: none;
  font-size: 1.25rem;
  // color: currentColor;
  color: #dedede;

  &:focus {
    outline: 5px auto dodgerblue;
  }
`

const Home: FC<HomeProps> = ({ className = '' }) => {
  return (
    <main className={`${className} page__home`}>
      <LinkButton
        className="button--login"
        as={Link}
        to="/sign-in"
        style={{ display: 'inline-block' }}
      >
        Sign In
      </LinkButton>
    </main>
  )
}

export default Home
