import { Button, TextField } from '@mui/material'
import React, { FC } from 'react'
import { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { IFormLogin } from '../@types/forms'
import BaseButton from '../components/BaseButton'
import { login } from '../services/api/auth'

import { useCustomerStore } from '../store/customers'

type LoginProps = {
  className?: string
}

const Login: FC<LoginProps> = ({ className = '' }) => {
  const [isLoading, setIsLoading] = useState(false)
  const { control, handleSubmit, formState } = useForm<IFormLogin>()

  const setToken = useCustomerStore(state => state.setToken)

  const navigate = useNavigate()

  const handleLogin = handleSubmit(async function handleLogin(
    formValues: IFormLogin,
  ) {
    try {
      setIsLoading(true)
      const loggedInToken = await login({
        username: formValues.email,
        password: formValues.password,
      })

      setToken(loggedInToken)
      localStorage.setItem('token', loggedInToken)

      navigate('/customers/me')
    } catch (error: unknown) {
      console.error('❌', error)
      alert('❌ Username or password is incorrect')
    } finally {
      setIsLoading(false)
    }
  })

  return (
    <main className={className}>
      <form onSubmit={handleLogin}>
        <Controller
          name="email"
          control={control}
          render={({ field }) => (
            <TextField
              label="Email"
              placeholder="email@example.com"
              variant="outlined"
              {...field}
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField label="Password" type="password" {...field} />
          )}
        />

        {/* TODO: Add Loading Indicator */}
        <BaseButton type="submit" disabled={isLoading}>
          Login
        </BaseButton>
      </form>
    </main>
  )
}

export default Login
