import React, { FC } from 'react'

type NotFoundProps = {
  className?: string
}

const NotFound: FC<NotFoundProps> = ({ className = '' }) => {

  return (
    <div className={className}>
      <p>Page not found</p>
    </div>
  )
}

export default NotFound
