import { IAuthRequest } from './../../@types/api/request/auth';
import axios from '../../configs/axios'

export async function login(requestBody: IAuthRequest): Promise<string> {
  const { data } = await axios.post('/integration/customer/token', requestBody)
  return data
}
