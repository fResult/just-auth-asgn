import { ICustomer } from '../../@types/customers'
import axios from '../../configs/axios'

export async function fetchCustomerDetail(token: string): Promise<ICustomer> {
  const { data } = await axios.get<ICustomer>('/customers/me', {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })

  return data
}
