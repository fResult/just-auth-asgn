import { ICustomer } from './../@types/customers'
import { create } from 'zustand'
import { devtools, persist } from 'zustand/middleware'
import { Nullable } from '../@types'

interface ICustomerState {
  token: string
  customer: Nullable<ICustomer>
  setToken: (token: string) => void
  setCustomer: (customer: ICustomer) => void
}

const createCustomerStore = create<ICustomerState>()

export const useCustomerStore = createCustomerStore((set) => ({
  token: '',
  customer: null,
  setToken: (token) => set({ token }),
  setCustomer: (customer) => set({ customer }),
}))
